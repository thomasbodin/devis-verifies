/*!
 * DashCore - SaaS, App & Software Website template.
 * Main Javascript file
 *
 * This files serves as the main entry for all components (scripts and styling)
 * You should edit according to your own needs, remove the components you won't need (use) is recommended in order to get a smaller final bundle file
 * Author:  http://themeforest.net/user/5studiosnet
 * Copyright © 2024 5Studios.net
 * https://5studios.net
 **/

import 'bootstrap';
//import './app/js/page-loading.js';
import './app/js/feather.js';

// GENERAL JS AND STYLES
// FontAwesome used icons
import './app/js/font-awesome.js';
import './app/js/aos.js';

// Core components
import './app/js/navbar.js';
//import './app/js/main-menu.js';
import './app/js/scrolling.js';
import './app/js/scroll-to-top.js';
import './app/js/background.js';

// Initializing of the scripts used across the site
// Feel free to edit this section, you can add additional scripts you might need or remove what you won't use

import './app/js/swiper.js';
import './app/js/typed.js';
//import './app/js/pricing.js';
//import './app/js/lightbox.js';
import './app/js/syntax-highlighter.js';
import './app/js/counters.js';
//import './app/js/forms.js';
//import './app/js/wizard.js';
import './app/js/simplebar.js';

// For 'saas' page
//import './app/js/perspective-mockups';

// For 'integration' page
//import './app/js/integration-bubbles';

// Import styles at the end, so default plugins styles can be overridden when needed
//import '../sass/dashcore.scss';
